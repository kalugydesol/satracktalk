import React from 'react';
import './styles.css';
export default class Wellcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: "",
    };
  }

  componentDidMount() {
    console.log("MOUNTED");
    this.setState({ posts: "dato2" });
  }

  componentWillUnmount() {
    console.log("UNMOUNTED");
  }

  render() {
    return (
      <div className='wrapper'>
      <div className='form-wrapper'>
        <div className='form-wrapper2' > {this.props.message}</div>
        <div className={this.props.buttomStyle} > {this.props.buttomMessage}</div>
        <h2>{this.state.posts} </h2>
      </div>
      </div>
    );
  }
};