// import 'zone.js';
import * as singleSpa from 'single-spa'; 
import { loadApp } from './helper'

async function init() {

    // app1: The URL "/app1/..." is being redirected to "http://localhost:9001/..." this is done by the webpack proxy (webpack.config.js)
    await loadApp('app1', '/app1', '/app1/singleSpaEntry.js', null, null);
    await loadApp('app3', '/app3', '/app3/singleSpaEntry.js', null, null); 
    await loadApp('app4', '/app4', '/app4/singleSpaEntry.js', null, null); 
    await loadApp('app7', '/app7', '/main.js', null, null);
    singleSpa.start();
}

init();

