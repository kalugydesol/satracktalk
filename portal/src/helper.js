import * as singleSpa from 'single-spa'; // waiting for this to be merged: https://github.com/CanopyTax/single-spa/pull/156

export function hashPrefix(prefix) {
    return function (location) {
        return location.hash.startsWith(`#${prefix}`);
    }
}


export async function loadApp(name, hash, appURL, storeURL, globalEventDistributor) {
    let customProps = {test: "test"};

    // register the app with singleSPA 
    if(name =="/app1"){
        singleSpa.registerApplication(
            name,
            () => System.import(appURL),
            hashPrefix(hash), customProps
        );
    }
    else{
        singleSpa.registerApplication(name, () => SystemJS.import(appURL), hashPrefix(hash), customProps);

    }
}